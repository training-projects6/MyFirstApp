Purpose:


- We have a URL to get JSON with some data. We need to parse it.

- Use Core Data to save the results of the network call.

- Display a list of users with a little information about them, such as their name and whether they are active right now.

- Create a detail view shown when a user is tapped, presenting more information about them, including the names of their friends.




<img src="/uploads/062e2f40956fc5ce16380ffe07d247dd/Simulator_Screen_Shot_-_iPhone_14_Pro_-_2022-11-08_at_16.02.35.png" width="320" >

<img src="/uploads/27106afb9c3baeba09739e0acb2babbe/Simulator_Screen_Shot_-_iPhone_14_Pro_-_2022-11-08_at_16.02.43.png" width="320" >

