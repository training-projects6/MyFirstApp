//
//  MyFirstAppApp.swift
//  MyFirstApp
//
//  Created by Artem Soloviev on 29.09.2022.
//

import SwiftUI

@main
struct MyFirstAppApp: App {
    @StateObject private var dataController = DataController()
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, dataController.container.viewContext)
        }
    }
}
