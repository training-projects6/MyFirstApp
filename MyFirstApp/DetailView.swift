//
//  DetailView.swift
//  MyFirstApp
//
//  Created by Artem Soloviev on 11.10.2022.
//

import SwiftUI

struct DetailView: View {
    
    
    let user: CachedUser
    
    var body: some View {
        List {
            Section {
                
                Text("Age: \(user.age)")
                Text("Email: \(user.wrappedEmail)")
                Text("Address: \(user.wrappedAddress)")
                Text("Company: \(user.wrappedCompany)")
                
            } header: {
                Text ("Main info")
            }
            Section {
                ForEach (user.friendsArray) { friend in
                    
                    
                    Text(friend.wrappedName)
                    
                    
                }
                
            } header: {
                Text ("Friends list")
            }
            
            .navigationTitle(user.wrappedName)
            .navigationBarTitleDisplayMode(.large)
            
        }
        
    }
}


//struct DetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailView(user: User)
//    }
//}
