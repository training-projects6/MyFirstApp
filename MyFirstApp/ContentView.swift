//
//  ContentView.swift
//  MyFirstApp
//
//  Created by Artem Soloviev on 29.09.2022.
//

import SwiftUI

struct ContentView: View {
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(sortDescriptors: []) var cachedUser: FetchedResults<CachedUser>
    //    @FetchRequest(sortDescriptors: []) var cachedFriend: FetchedResults<CachedFriend>
    
    @State private var result = [User]()
    
    
    
    var body: some View {
        NavigationView {
            
            List (cachedUser, id: \.id) { user in
                NavigationLink {
                    DetailView(user: user)
                } label: {
                    
                    VStack(alignment: .leading) {
                        
                        Text(user.wrappedName)
                            .font(.headline)
                        
                        
                        Text(user.isActive ? "Active" : "Offline")
                        
                            .foregroundColor(user.isActive ? .green : .red)
                    }
                    
                }
                
                
                
                
            }
            .task {
                if cachedUser.isEmpty {
                    await loadData()
                    
                    await MainActor.run {
                        for user in result {
                            let newUser = CachedUser(context: moc)
                            
                            newUser.id = user.id
                            newUser.name = user.name
                            newUser.address = user.address
                            newUser.email = user.email
                            newUser.age = Int16(user.age)
                            newUser.isActive = user.isActive
                            newUser.company = user.company
                            
                            
                            for friend in user.friends {
                                let newFriend = CachedFriend(context: moc)
                                newFriend.name = friend.name
                                newFriend.id = friend.id
                                newUser.addToFriends(newFriend)
                                
                            }
                            
                        }
                        try? moc.save()
                        
                    }
                    
                }
            }
        }
    }
    func loadData () async {
        guard let url = URL(string: "https://www.hackingwithswift.com/samples/friendface.json") else {
            print ("invalid URL")
            return
        }
        
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
    
            
            
            let decodedResult = try JSONDecoder().decode([User].self, from: data)
            result = decodedResult
            
            
        }catch{
            print ("invalid data")
            print("Unexpected error: \(error).")
            
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
